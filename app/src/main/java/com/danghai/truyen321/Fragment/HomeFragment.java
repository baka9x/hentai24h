package com.danghai.truyen321.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.truyen321.R;
import com.danghai.truyen321.Adapter.ImageRecyclerViewAdapter;
import com.danghai.truyen321.Model.Comic;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class HomeFragment extends Fragment {

    final String HOME_URL = "https://truyen321.net/moi";
    RecyclerView rvMoiCapNhat;
    ImageRecyclerViewAdapter moiCapNhatAdapter;


    ArrayList<Comic> mList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        rvMoiCapNhat = view.findViewById(R.id.rv_moi_cap_nhat);
        rvMoiCapNhat.setHasFixedSize(true);
        //Gridlayout
        rvMoiCapNhat.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mList = new ArrayList<>();

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, HOME_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Trong day sẽ xử lý bóc tách html
                String url = "";
                String img = "";
                String title = "";
                String prefix = "NEW";
                String chapter = "";
                Document document = Jsoup.parse(response);
                if (document != null) {
                    //<div class="content-child"> ==> div[class=content-child] == div.content-child
                    //Element element = document.getElementById("containerListPage");
                    Elements elements = document.select("div.col-md-3.col-sm-6.col-xs-6.home-truyendecu");
                    //Elements elements = element.select("img");

                    for (Element element : elements) {
                        Element elementUrl = element.getElementsByTag("a").first();
                        Element elementImg = element.getElementsByTag("img").first();
                        Element elementTitle = element.getElementsByTag("h3").first();
                        //Element elementPrefix = element.getElementsByTag("small").first();
                        Element elementChapter = element.getElementsByTag("small").first();
                        if (elementUrl != null) {
                            url = elementUrl.attr("href");

                        }
                        if (elementTitle != null) {
                            title = elementTitle.text();

                        }
                        if (elementChapter != null) {
                            chapter = elementChapter.text();

                        }
                        if (elementImg != null) {
                            img = elementImg.attr("src");

                        }

                        mList.add(new Comic(url, img, title, chapter, prefix));

                    }
                    moiCapNhatAdapter = new ImageRecyclerViewAdapter(getContext(), mList);
                    rvMoiCapNhat.setAdapter(moiCapNhatAdapter);
                    moiCapNhatAdapter.notifyDataSetChanged();

                    moiCapNhatAdapter.setOnItemClickListener(new ImageRecyclerViewAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Bundle bundle = new Bundle();

                            Comic comic = mList.get(position);
                            String urls = comic.getUrl();
                            bundle.putString("Data-href", urls);
                            Fragment fragment = new TopicFragment();
                            fragment.setArguments(bundle);
                            replaceFragment(fragment);

                        }
                    });

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        requestQueue.add(stringRequest);
        return view;
    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
