package com.danghai.truyen321.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.truyen321.R;
import com.danghai.truyen321.Adapter.ChapterAdapter;
import com.danghai.truyen321.Model.Category;
import com.danghai.truyen321.Model.Chapter;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class TopicFragment extends Fragment {


    ImageView ivThumbnail;

    TextView tvTitle, tvAuthor, tvContent;

    RecyclerView rvCategory, rvChapter;

    ArrayList<Category> mCategory;
    ArrayList<Chapter> mChapter;

    ChapterAdapter chapterAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_topic, container, false);
        final String TOPIC_URL = getArguments().getString("Data-href");
        //Ánh xạ
        ivThumbnail = view.findViewById(R.id.iv_thumbnail_topic);
        tvTitle = view.findViewById(R.id.tv_title_topic);
        tvAuthor = view.findViewById(R.id.tv_author_topic);
        tvContent = view.findViewById(R.id.tv_content_topic);
        rvCategory = view.findViewById(R.id.rv_category_topic);
        rvChapter = view.findViewById(R.id.rv_chapter_topic);

        rvCategory.setHasFixedSize(true);
        rvChapter.setHasFixedSize(true);
        //Gridlayout
        rvChapter.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mCategory = new ArrayList<>();
        mChapter = new ArrayList<>();


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, TOPIC_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Trong day sẽ xử lý bóc tách html
                String url = "";
                String img = "";
                String title = "";
                String prefix = "NEW";
                String author = "";
                String content = "";
                String titleChapter = "";
                Document document = Jsoup.parse(response);
                if (document != null) {

                    Elements elements = document.select("div.col-xs-12.col-info-desc");
                    Element elements1 = document.getElementById("list-chapter");
                    Elements elements2 = elements1.select("a");

                    for (Element element : elements) {
                        Element elementImg = element.getElementsByTag("img").first();
                        Element elementTitle = element.getElementsByTag("h2").first();
                        Element elementContent = element.getElementsByClass("desc-text").first();

                        if (elementTitle != null) {
                            title = elementTitle.text();
                            tvTitle.setText(title);
                        }
                        if (elementContent != null) {
                            content = elementContent.text();
                            tvContent.setText(content);

                        }

                        if (elementImg != null) {
                            img = elementImg.attr("src");
                            Picasso.get().load(img).into(ivThumbnail);

                            //Proxy: https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&gadget=a&no_expand=1&refresh=604800&url=
                            //img = "https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&gadget=a&no_expand=1&refresh=604800&url=" + img;
                        }


                    }
                    for (Element element2 : elements2){
                        Element elementurl = element2.getElementsByTag("a").first();

                        if (elementurl != null){
                            url = elementurl.attr("href");
                            titleChapter = elementurl.attr("title");

                        }

                        mChapter.add(new Chapter(url, titleChapter));
                    }




                    chapterAdapter = new ChapterAdapter(getContext(), mChapter);
                    rvChapter.setAdapter(chapterAdapter);
                    chapterAdapter.notifyDataSetChanged();
                    chapterAdapter.setOnItemClickListener(new ChapterAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Bundle bundle = new Bundle();

                            Chapter chapter = mChapter.get(position);
                            String urls = chapter.getUrl();
                            bundle.putString("Chapter-href", urls);
                            Fragment fragment = new ChapterFragment();
                            fragment.setArguments(bundle);
                            replaceFragment(fragment);
                            Log.d("TitleChapter", urls);
                        }
                    });


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        requestQueue.add(stringRequest);
        return view;
    }


    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
