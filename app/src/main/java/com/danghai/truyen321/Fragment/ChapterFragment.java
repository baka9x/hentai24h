package com.danghai.truyen321.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.truyen321.Adapter.ChapterViewAdapter;
import com.danghai.truyen321.Model.ChapterImage;
import com.danghai.truyen321.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;


public class ChapterFragment extends Fragment {

    TextView tvContent;
    ChapterViewAdapter chapterViewAdapter;
    ArrayList<ChapterImage> chapterImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chapter, container, false);
        final String CHAPTER_URL = getArguments().getString("Chapter-href");
        //ÁNH XẠ
        tvContent = view.findViewById(R.id.tv_content);

        chapterImage = new ArrayList<>();

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CHAPTER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String content = "";

                Document document = Jsoup.parse(response);
                if (document != null) {

                    Elements elements = document.getElementsByClass("chapter-content");
                    content = elements.text();


                    tvContent.setText(content);

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(stringRequest);
        return view;
    }

}
