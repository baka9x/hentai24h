package com.danghai.truyen321.Model;

public class Category {

    private String url;
    private String title;

    public Category(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public Category() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
