package com.danghai.truyen321.Model;

public class ChapterImage {

    private String img;

    public ChapterImage() {
    }

    public ChapterImage(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
