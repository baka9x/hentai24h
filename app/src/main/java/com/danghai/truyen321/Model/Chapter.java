package com.danghai.truyen321.Model;

public class Chapter {

    private String url;
    private String title;

    public Chapter(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public Chapter() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
