package com.danghai.truyen321.Model;

public class Topic {

    private String url;
    private String thumbnail;
    private String title;
    private String content;
    private String chapterUrl;
    private String chapterName;
    private String categoryUrl;
    private String categoryName;
    private String author;
    private String status;
    private String prefix;
    private String view;

    public Topic(String url, String thumbnail, String title, String content, String chapterUrl, String chapterName, String categoryUrl, String categoryName, String author, String status, String prefix, String view) {
        this.url = url;
        this.thumbnail = thumbnail;
        this.title = title;
        this.content = content;
        this.chapterUrl = chapterUrl;
        this.chapterName = chapterName;
        this.categoryUrl = categoryUrl;
        this.categoryName = categoryName;
        this.author = author;
        this.status = status;
        this.prefix = prefix;
        this.view = view;
    }

    public Topic() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getChapterUrl() {
        return chapterUrl;
    }

    public void setChapterUrl(String chapterUrl) {
        this.chapterUrl = chapterUrl;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
}
