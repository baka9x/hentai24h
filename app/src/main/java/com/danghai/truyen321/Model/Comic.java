package com.danghai.truyen321.Model;

public class Comic {

    private String url;
    private String thumbnail;
    private String title;
    private String chapter;
    private String prefix;

    public Comic(String url, String thumbnail, String title, String chapter, String prefix) {
        this.url = url;
        this.thumbnail = thumbnail;
        this.title = title;
        this.chapter = chapter;
        this.prefix = prefix;
    }

    public Comic() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }


}
