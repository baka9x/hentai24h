package com.danghai.truyen321.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.truyen321.Model.ChapterImage;
import com.danghai.truyen321.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChapterViewAdapter extends RecyclerView.Adapter<ChapterViewAdapter.ChapterViewHolder> {

    private Context mContext;
    private ArrayList<ChapterImage> mList;

    public ChapterViewAdapter(Context mContext, ArrayList<ChapterImage> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ChapterViewAdapter.ChapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter_image, parent, false);


        return new ChapterViewAdapter.ChapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChapterViewAdapter.ChapterViewHolder holder, int position) {
        ChapterImage chapterImage = mList.get(position);

        Picasso.get().load(chapterImage.getImg()).fit().into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ChapterViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ChapterViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.iv_chapter_image);
        }
    }
}
