package com.danghai.truyen321.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.truyen321.Model.Chapter;
import com.danghai.truyen321.R;


import java.util.ArrayList;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ChapterHolder> {

    private Context mContext;
    private ArrayList<Chapter> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    public ChapterAdapter(Context mContext, ArrayList<Chapter> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ChapterAdapter.ChapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter_topic, parent, false);


        return new ChapterAdapter.ChapterHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ChapterAdapter.ChapterHolder holder, int position) {
        Chapter chapter = mList.get(position);

        holder.tvTitle.setText(chapter.getTitle());


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ChapterHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        public ChapterHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title_chapter);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
