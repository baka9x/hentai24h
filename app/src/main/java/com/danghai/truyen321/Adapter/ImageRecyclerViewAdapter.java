package com.danghai.truyen321.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.truyen321.Model.Comic;
import com.danghai.truyen321.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageRecyclerViewAdapter.ImageHolder> {

    private Context mContext;
    private ArrayList<Comic> mList;
    private OnItemClickListener mListener;


    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    public ImageRecyclerViewAdapter(Context mContext, ArrayList<Comic> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ImageRecyclerViewAdapter.ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Call view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);

        return new ImageRecyclerViewAdapter.ImageHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageRecyclerViewAdapter.ImageHolder holder, int position) {
        //Put data

        Comic comic = mList.get(position);

        //Using picasso to load to imageview
        holder.tvTitle.setText(comic.getTitle());
        holder.tvPrefix.setText(comic.getPrefix());
        holder.tvChapter.setText(comic.getChapter());

        Picasso.get().load(comic.getThumbnail()).fit().into(holder.ivMangaImg);


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ImageHolder extends RecyclerView.ViewHolder{

        ImageView ivMangaImg;
        TextView tvTitle, tvPrefix, tvChapter;


        public ImageHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            //Ánh xạ
            ivMangaImg = itemView.findViewById(R.id.iv_manga_img);
            tvTitle = itemView.findViewById(R.id.tv_manga_title);
            tvPrefix = itemView.findViewById(R.id.tv_prefix);
            tvChapter = itemView.findViewById(R.id.tv_manga_chapter);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }



}
